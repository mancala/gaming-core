// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "GamingCore",
    products: [
        .library(
            name: "GamingCore",
            targets: ["GamingCore"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:msanford1030/SoS.git", from: "1.2.0"),
        .package(url: "git@gitlab.com:msanford1030/NetworkingCore.git", from: "1.2.0"),
    ],
    targets: [
        .target(
            name: "GamingCore",
            dependencies: ["SwiftOnSockets", "NetworkingCore"])
    ]
)
