//
//  GameCoding.swift
//  manserver
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore

extension GameToken: ByteCoding {
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(gameID: gameID)
        archiver.encode(watermark: watermark)
        archiver.encode(userID: playerID)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.gameID = try unarchiver.decodeGameID()
            self.watermark = try unarchiver.decodeWatermark()
            self.playerID = try unarchiver.decodeUserID()
        } catch {
            assert(false, "unarchiving of GameToken failed")
            return nil
        }
    }
}

/*----------------------------------------------------------*/

public extension ByteArchiver {
    func encode(gameID: GameID) {
        guard gameID <= Int(UInt16.max) else {
            assert(false, "invalid gameID. cannot encode.")
            return
        }
        encode(UInt16(gameID))
    }
    
    func encode(watermark: Watermark) {
    guard watermark <= Int(UInt32.max) else {
            assert(false, "invalid watermark. cannot encode.")
            return
        }
        encode(UInt32(watermark))
    }
    
    func encode(userID: UserID) {
        guard userID <= Int(UInt8.max) else {
            assert(false, "invalid watermark. cannot encode.")
            return
        }
        encode(UInt8(userID))
    }
}

public extension ByteUnarchiver {
    func decodeGameID() throws -> GameID {
        return GameID(try decodeUInt16())
    }
    
    func decodeWatermark() throws -> Watermark {
        return Watermark(try decodeUInt32())
    }
    
    func decodeUserID() throws -> UserID {
        return UserID(try decodeUInt8())
    }
}
