//
//  GameTypes.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets
import NetworkingCore

public typealias GameID = Int
public typealias PlayerID = UserID

public enum UserType {
    case spectator
    case player(PlayerID)
}

public struct GameToken: Hashable, CustomStringConvertible, CustomDebugStringConvertible {
    public let gameID: GameID
    public let watermark: Watermark
    public let playerID: PlayerID
    
    public init(gameID: GameID, watermark: Watermark, playerID: PlayerID) {
        self.gameID = gameID
        self.watermark = watermark
        self.playerID = playerID
    }
    
    public var hashValue: Int {
        return watermark
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ [GameToken] gameID=\(gameID) | watermark=\(watermark) | playerID=\(playerID) }"
    }
}

public func ==(lhs: GameToken, rhs: GameToken) -> Bool {
    return (lhs.gameID == rhs.gameID) && (lhs.playerID == rhs.playerID) && (lhs.watermark == rhs.watermark)
}

public struct GameAddress {
    public let host: IPAddress
    public let token: GameToken
    
    public init(host: IPAddress, token: GameToken) {
        self.host = host
        self.token = token
    }
}

public extension Timestamp64 {
    var watermark: Watermark {
        return Watermark(UInt32(self))
    }
}

public extension PlayerID {
    static let max = 32
    static let spectator = PlayerID(UInt8.max)
    
    static let player1 = 0
    static let player2 = 1
    static let player3 = 2
    static let player4 = 3
}

/*-------------------*/

public protocol GameTimerProtocol {
    var timeInterval: TimeInterval { get }
    var fireDate: Date { get }
    var isValid: Bool { get }
    func invalidate()
    func fireIfNeeded()
}

public protocol GameContainerProtocol {
    func scheduleTimer(withTimeInterval timeInterval: TimeInterval, repeats: Bool, callback: @escaping () -> ()) -> GameTimerProtocol
}
