//
//  GameMessage.swift
//
//  Created by Michael Sanford on 12/22/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import SwiftOnSockets

public enum ClientToLobbyMessageType: MessageType {
    case createGameRequest = 50
    case cancelCreateGameRequest = 51
}

public enum LobbyToClientMessageType: MessageType {
    case createGameResponse = 70
}

public enum ClientToGameServerMessageType: MessageType {
    case gameToken = 150
    case evictPlayer = 151
    case joinGame = 152
}

/*--------------------------------------------*/

public struct ClientGameRequest: ByteCoding {
    public let type = ClientToLobbyMessageType.createGameRequest
    
    public let requestID: UUID
    public let gameCriteria: Data
    
    public init(requestID: UUID, gameCriteria: Data) {
        self.requestID = requestID
        self.gameCriteria = gameCriteria
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.requestID = try unarchiver.decodeUUID()
            self.gameCriteria = try unarchiver.decodeData()
        } catch {
            assert(false, "failed to unarchive ClientGameRequest due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(requestID)
        archiver.encode(gameCriteria)
    }
}

/*--------------------------------------------*/

public struct ClientGameResponse: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public enum Result {
        case success(IPAddress, GameToken, Data)
        case failure(String)
    }
    
    public let type = LobbyToClientMessageType.createGameResponse
    
    public let requestID: UUID
    public let result: Result
    
    public init(requestID: UUID, gameServerAddress: IPAddress, gameToken: GameToken, gameInfo: Data) {
        self.requestID = requestID
        self.result = .success(gameServerAddress, gameToken, gameInfo)
    }
    
    public init(requestID: UUID, errorText: String) {
        self.requestID = requestID
        self.result = .failure(errorText)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.requestID = try unarchiver.decodeUUID()
            let isSuccess: Bool = try unarchiver.decodeBool()
            if isSuccess {
                let gameServerAddress = try unarchiver.decodeIPAddress()
                guard let gameToken = GameToken(unarchiver: unarchiver) else {
                    assert(false, "failed to unarchive ClientGameResponse due to bad format")
                    return nil
                }
                let gameInfo: Data = try unarchiver.decodeData()
                result = .success(gameServerAddress, gameToken, gameInfo)
            } else {
                let errorText: String = try unarchiver.decodeString()
                result = .failure(errorText)
            }
        } catch {
            assert(false, "failed to unarchive ClientGameResponse due to bad format")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(requestID)
        switch result {
        case .success(let gameServerAddress, let gameToken, let gameInfo):
            archiver.encode(true)
            archiver.encode(gameServerAddress)
            archiver.encode(gameToken)
            archiver.encode(gameInfo)
        case .failure(let errorText):
            archiver.encode(false)
            archiver.encode(errorText)
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch result {
        case .success(let gameServerAddress, let gameToken, let gameInfo):
            return "{ [ClientGameResponse] success; gameServerAddress=\(gameServerAddress) | gameToken=\(gameToken) | gameInfo.count=\(gameInfo.count) }"
        case .failure(let errorText):
            return "{ [ClientGameResponse] failure; errorText=\(errorText) }"
        }
    }
}

/*--------------------------------------------*/

public struct GameMessage: ByteCoding {
    public static let type = ClientToGameServerMessageType.gameToken
    
    public let gameToken: GameToken
    public let message: Message
    
    public init(gameToken: GameToken, type: MessageType, payload: Data?) {
        self.gameToken = gameToken
        self.message = Message(type: type, payload: payload)
    }
    
    public init(gameToken: GameToken, message: Message) {
        self.gameToken = gameToken
        self.message = message
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(gameToken)
        archiver.encode(message)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        guard let gameToken = GameToken.init(unarchiver: unarchiver) else { return nil }
        guard let message = Message.init(unarchiver: unarchiver) else { return nil }
        self.gameToken = gameToken
        self.message = message
    }
}

/*--------------------------------------------*/

public struct PlayerJoinGameRequest: ByteCoding {
    public static let type = ClientToGameServerMessageType.joinGame
    
    public let identifier: UUID
    public let gameToken: GameToken
    
    public init(identifier: UUID = UUID(), gameToken: GameToken) {
        self.identifier = identifier
        self.gameToken = gameToken
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.identifier = try unarchiver.decodeUUID()
        } catch {
            assert(false, "failed to unarchive PlayerJoinGameRequest")
            return nil
        }
        
        guard let gameToken = GameToken(unarchiver: unarchiver) else {
            assert(false, "failed to unarchive PlayerJoinGameRequest")
            return nil
        }
        self.gameToken = gameToken
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(identifier)
        archiver.encode(gameToken)
    }
}

/*--------------------------------------------*/

public extension Message {
    init(gameMessage: GameMessage) {
        let archiver = ByteArchiver()
        gameMessage.encode(with: archiver)
        self.init(type: GameMessage.type.rawValue, payload: archiver.archive)
    }
    
    init(type: MessageType, payloadInfo: ByteCoding?) {
        let payload: Data?
        if let payloadInfo = payloadInfo {
            payload = ByteArchiver.archive(for: payloadInfo)
        } else {
            payload = nil
        }
        self.init(type: type, payload: payload)
    }
}
